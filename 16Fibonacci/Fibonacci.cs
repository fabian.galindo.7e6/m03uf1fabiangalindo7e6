﻿/*
* AUTHOR: Fabian Galindo de Oliveira
* DATE: 2022/10/25
* DESCRIPTION: Donat un nombre enter positiu, escriu els nombres de Fibonacci.
*/
using System;

namespace _16Fibonacci
{
    internal class Fibonacci
    {
        static void Main(string[] args)
        {
            int i, j, k;

            i= 0;
            j= 1;
        
            while (i < 1000)
            {
                Console.WriteLine(i);
                k = i + j;
                j = i;
                i = k;
            }
        }
    }
}
