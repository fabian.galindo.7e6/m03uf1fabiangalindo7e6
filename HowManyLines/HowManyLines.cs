﻿/*
* AUTHOR: Fabian Galindo de Oliveira
* DATE: 2022/10/05
* DESCRIPTION: Volem comptar quantes línies té un text introduït per l'usuari.
*/
using System;

namespace HowManyLines
{
    class HowManyLines
    {
        static void Main(string[] args)
        {
            int i = 0;
            string text = "END";

            Console.WriteLine("Escriu totes les linees que vulguis, en acabar introdueix la comanda END per finalitzar.");
            string textUsuari = Console.ReadLine();

            while(text != textUsuari) 
            {
                i++;
                textUsuari = Console.ReadLine();
            }

            Console.WriteLine("Has escrit {0} línies", i);

        }
    }
}
