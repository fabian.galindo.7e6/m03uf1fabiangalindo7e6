﻿/*
* AUTHOR: Fabian Galindo de Oliveira
* DATE: 2022/10/05
* DESCRIPTION: Demanar a l'usuari un enter entre 1 i 5. Si introdueix un número més gran o més petit, torna-li a demanar. 
* Mostra per pantalla: El número introduït: 3, (en cas de que l'usuari hagi introduït un 3) substituint el 3 pel número.
*/
using System;

namespace NumberBetweenOneAndFive
{
    class NumberBetweenOneAndFive
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Introdueix un número entre el 1 i 5");

            int numUsuari = Convert.ToInt32(Console.ReadLine());

            while(numUsuari < 1 || numUsuari > 5) 
            {
                numUsuari = Convert.ToInt32(Console.ReadLine());
            }

            Console.WriteLine("El teu número es: {0}", numUsuari);
        }
    }
}
