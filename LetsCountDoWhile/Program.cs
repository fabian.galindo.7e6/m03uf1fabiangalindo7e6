﻿/*
* AUTHOR: Fabian Galindo de Oliveira
* DATE: 2022/10/05
* DESCRIPTION: Mostra per pantalla tots els números fins a un enter entrat per l'usuari.
* Versió Do While
*/
using System;

namespace LetsCountDoWhile
{
    class Program
    {
        static void Main(string[] args)
        {
            int num = 0;
            int numUsuari = Convert.ToInt32(Console.ReadLine());

            do
            {
                num++;
                Console.Write(num + " ");

            }   while (num < numUsuari);

        }
    }
}
