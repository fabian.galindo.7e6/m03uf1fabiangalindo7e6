﻿/*
* AUTHOR: Fabian Galindo de Oliveira
* DATE: 2022/10/05
* DESCRIPTION: Mostra per pantalla tots els números fins a un enter entrat per l'usuari.
*/
using System;

namespace LetsCount
{
    class LetsCount
    {
        static void Main(string[] args)
        {
            int num = 0;
            Console.Write("Introdueix un enter: ");
            int numUsuari = Convert.ToInt32(Console.ReadLine());

            while (num < numUsuari) 
            {
                num++;
                Console.Write(num + " ");
            }
        }
    }
}
