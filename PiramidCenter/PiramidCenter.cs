﻿/*
* AUTHOR: Fabian Galindo de Oliveira
* DATE: 2022/10/25
* DESCRIPTION: S'imprimeix una piràmide d'altura N de # centrada
*/
using System;

namespace PiramidCenter
{
    internal class PiramidCenter
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Introdueix l'altura de la piràmide");
            int altura = Convert.ToInt32(Console.ReadLine());

            for (int i = 1; i <= altura; i++)
            {
                for (int espai = 1; espai <= (altura - i); espai++)
                    Console.Write(" ");

                for (int col = 1; col < i * 2; col++)
                    Console.Write("#");
                Console.WriteLine();
            }
        }
    }
}
