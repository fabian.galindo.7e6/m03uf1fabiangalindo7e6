﻿/*
* AUTHOR: Fabian Galindo de Oliveira
* DATE: 2022/10/25
* DESCRIPTION: Imprimeix les taules de multiplicar en forma de taula
*/

using System;

namespace _13MultiplyTableFull
{
    internal class MultiplyTableFull
    {
        static void Main(string[] args)
        {
            int i;
            int j;

            //Realitzar el producte cartesià dels conjunts taules y nùmeros.
            for (i = 1; i <= 10; i++)
            {
                Console.WriteLine($"Tabla de multiplicar del {i}:");

                for (j = 1; j <= 10; j++)
                {
                    Console.WriteLine($"{i} x {j} = {i * j}");
                }
            }
        }
    }
}
