﻿/*
* AUTHOR: Fabian Galindo de Oliveira
* DATE: 2022/10/25
* DESCRIPTION: Calcula el factorial d'un valor
*/
using System;

namespace _15Factorial
{
    internal class Factorial
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Introdueix un nombre per mostrar el seu factorial.");
            int num = Convert.ToInt32(Console.ReadLine());
            int numFactorial = 1;

            for (int i = 1; i <= num; i++)
            {
                numFactorial *= i;
            }
            Console.WriteLine(numFactorial);
        }
    }
}
