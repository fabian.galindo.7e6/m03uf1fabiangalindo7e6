﻿/*
* AUTHOR: Fabian Galindo de Oliveira
* DATE: 2022/10/25
* DESCRIPTION: Escriu un programa que llegeixi un nombre natural més petit que 256 i escrigui la seva representació en binari.
*/
using System;

namespace _17ToBinnary
{
    internal class ToBinnary
    {
        static void Main(string[] args)
        {
            Console.Write("Nombre natural menor a 256: ");
            int numNatural = Convert.ToInt32(Console.ReadLine());

            int contador;
            string resultat = null;

            if (numNatural < 256)
            {
                do
                {
                    contador = numNatural % 2;
                    numNatural /= 2;
                    resultat = Convert.ToString(contador) + resultat;
                } while (numNatural > 0);

                Console.WriteLine("Numbre binari: {0}", resultat);
            } else
            {
                Console.WriteLine("No has entrat un nombre menor a 256.");
            }
        }
    }
}
