﻿/*
* AUTHOR: Fabian Galindo de Oliveira
* DATE: 2022/10/11
* DESCRIPTION: Printa per pantalla tots els valors que hi ha entre els dos valors introduits ordenats de menor a major.
*/
using System;

namespace LetsCountBetween
{
    class LetsCountBetween
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Introdueix dos valors, per pantalla veuras tots els valors que hi ha entre ells, ordenats de menor a major. ");
            Console.Write("Valor A: ");
            int numA = Convert.ToInt32(Console.ReadLine());
            Console.Write("Valor B: ");
            int numB = Convert.ToInt32(Console.ReadLine());

            if (numA < numB)
            {
                for(int i = (numB - numA) + 1; i < numB; i++)
                {
                    Console.Write(i);
                }
            }
            else
            {
                int numC = numA;
                numA = numB;
                numB = numC;

                for (int i = (numB - numA) + 1; i < numB; i++)
                {
                    Console.Write(i);
                }
            }
        }
    }
}
