﻿/*
* AUTHOR: Fabian Galindo de Oliveira
* DATE: 2022/10/11
* DESCRIPTION: Escriu tots els numeros des de l'1 fins al final, amb una distància de salt.
*/
using System;

namespace CountWithJumps
{
    class CountWithJumps
    {
        static void Main(string[] args)
        {
            //int i;
            Console.WriteLine("Amb els dos valor que introduexis et mostrara un salt fins arribar a ell.");
            int numFinal = Convert.ToInt32(Console.ReadLine());
            int numSalt = Convert.ToInt32(Console.ReadLine());

            for(int i = 1; i <= numFinal ; i += numSalt)
            {
                Console.Write("{0} ", i);
            }
        }
    }
}
