﻿/*
* AUTHOR: Fabian Galindo de Oliveira
* DATE: 2022/10/25
* DESCRIPTION: Implementa l'algorisme d'Euclides, que calcula le màxim comú divisor de dos nombres positius.
*/
using System;

namespace _19Euclides
{
    internal class Euclides
    {
        static void Main(string[] args)
        {
            int numA, numB, residu;
            Console.WriteLine("Teorema d'Euclides per calcular el mcd de dos nombre positus.");
            numA = Convert.ToInt32(Console.ReadLine());
            numB = Convert.ToInt32(Console.ReadLine());

            while (numB != 0)
            {
                residu = numA % numB;
                numA = numB;
                numB = residu;
            }
            Console.WriteLine(numA);
        }
    }
}
