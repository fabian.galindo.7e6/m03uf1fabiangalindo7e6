﻿/*
* AUTHOR: Fabian Galindo de Oliveira
* DATE: 2022/10/13
* DESCRIPTION: Necessitem fer un programa que donada una llista de puntuacions, ens digui qui és el guanyador del torneig.
*/
using System;

namespace GoTournamentGreatestScore
{
    class GoTournamentGreatestScore
    {
        static void Main(string[] args)
        {
            string initialPlayer = null;
            int initialPlayerScore = 0;

            Console.WriteLine("Nom del jugador, escriure END per mostrar el guanyador.");
            string newPlayer = Convert.ToString(Console.ReadLine());
            int newPlayerScore = 0;

            while (newPlayer != "END")
            {
                Console.WriteLine("Puntuació " + newPlayer);
                newPlayerScore = Convert.ToInt32(Console.ReadLine());

                if (newPlayerScore > initialPlayerScore)
                {
                    initialPlayer = newPlayer;
                    initialPlayerScore = newPlayerScore;
                }

                Console.WriteLine("Nom jugador");
                //Flag en caso de escribir END.
                newPlayer = Convert.ToString(Console.ReadLine());
            }

            Console.WriteLine(initialPlayer + " " + initialPlayerScore);
        }
    }
}
