﻿/*
* AUTHOR: Fabian Galindo de Oliveira
* DATE: 2022/10/05
* DESCRIPTION: Mostra per pantalla les taules de multiplicar printat per l'usuari.
* Versió Do While
*/
using System;

namespace MultiplyTableDoWhile
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Introdueix el numero per fer la taula: ");

            int i = 0;
            int num = Convert.ToInt32(Console.ReadLine());

            do
            {
                i++;
                int resultat = i * num;
                Console.WriteLine("{0} * {1} = {2}", i, num, resultat);
            }   while (i < 10);
        }
    }
}
