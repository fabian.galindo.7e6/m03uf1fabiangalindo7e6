﻿/*
* AUTHOR: Fabian Galindo de Oliveira
* DATE: 2022/10/25
* DESCRIPTION: Implementa un programa que mostre true si el nombre introduït és primer, false en qualsevol altre cas.
*/
using System;

namespace _20IsPrime
{
    internal class IsPrime
    {
        static void Main(string[] args)
        {
            Console.Write("Introdueix un nombre: ");

            int num = Convert.ToInt32(Console.ReadLine());
            bool primer = true;

            for (int i = 2; i < num / 2; i++)
            {
                if (num % i == 0)
                {
                    primer = false;
                    break;
                }
            }
            if (primer)
            {
                Console.Write("El nombre es primer.");
            }
            else
            {
                Console.Write("El nombre no es primer.");
            }           
        }
    }
}
