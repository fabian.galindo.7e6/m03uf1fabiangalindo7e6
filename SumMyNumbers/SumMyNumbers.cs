﻿/*
* AUTHOR: Fabian Galindo de Oliveira
* DATE: 2022/10/20
* DESCRIPTION: Fer un programa que llegeixi un nombre enter positiu n i escrigui la suma de les seves xifres.
*/
using System;

namespace SumMyNumbers
{
    class SumMyNumbers
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Entra un enter positiu.");

            int numUsuari = Convert.ToInt32(Console.ReadLine());
            int numSum = 0;

            int numResidu;
            //Operar amb int per obtenir els enters.
            while (numUsuari > 0)
            {
                //Donarà com a residu l'últim digit.
                numResidu = numUsuari % 10;
                numSum += numResidu;
                //Descartarà l'últim digit del sistema decimal.
                numUsuari = numUsuari / 10;
            }

            Console.WriteLine("La suma de les xifres es: " + numSum);
        }
    }
}
