﻿/*
* AUTHOR: Fabian Galindo de Oliveira
* DATE: 2022/10/11
* DESCRIPTION:Donada una llista de lletres, imprimeix únicament les vocals que hi hagi.
*/
using System;

namespace OnlyVowels
{
    class OnlyVowels
    {
        static void Main(string[] args)
        {

            Console.Write("Introdueix la quantitat de lletres: ");
            int numUsuari = Convert.ToInt32(Console.ReadLine());
            

            Console.WriteLine("Introdueix les lletres, despres es mostrarà les vocals incloses.");
            for(int i= 0; i < numUsuari; i++)
            {
                char lletraUsuari = Convert.ToChar(Console.Read());
                if (lletraUsuari == 'a' || lletraUsuari == 'e' || lletraUsuari == 'i' || lletraUsuari == 'o' || lletraUsuari == 'u' || 
                    lletraUsuari == 'A' || lletraUsuari == 'E' || lletraUsuari == 'I' || lletraUsuari == 'O' || lletraUsuari == 'U')
                {
                    Console.WriteLine(lletraUsuari);
                }
            }

        }
    }
}
