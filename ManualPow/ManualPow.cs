﻿/*
* AUTHOR: Fabian Galindo de Oliveira
* DATE: 2022/10/20
* DESCRIPTION: L'usuari introdueix dos enters, la base i l'exponent. Imprimeix el resultat de la potencia.
* (sense usar la llibreria math)
*/
using System;

namespace ManualPow
{
    class ManualPow
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Calculadora de potencies");
            Console.Write("Base: ");
            int numBase = Convert.ToInt32(Console.ReadLine());
            Console.Write("Exponent: ");
            int numExponent = Convert.ToInt32(Console.ReadLine());

            int calcul = 1;

            for (int i=0; i<numExponent; i++)
            {
                calcul *= numBase;
            }

            numBase = calcul;
            Console.WriteLine($"Potencia: {numBase}");

        }
    }
}
