﻿/*
* AUTHOR: Fabian Galindo de Oliveira
* DATE: 2022/10/25
* DESCRIPTION: Imprimeix tots els nombres enters divisibles per 3 que hi ha entre A i B (inclusiu).
*/
using System;

namespace _18DivisibleBy
{
    internal class DivisibleBy
    {
        static void Main(string[] args)
        {
            int i, m, n;
            Console.WriteLine("Entra dos nombres, et mostrara tots el divisibles per 3 entre ells dos (inclosos).");
            m = Convert.ToInt32(Console.ReadLine());
            n = Convert.ToInt32(Console.ReadLine());

            if (m > n)
            {
                int k = n;
                n = m;
                m = k;
            }

            int distancia = n - m;

            for (i = 0; i < distancia; i++)
            {
                int operacio = m % 3;
                if (operacio == 0)
                {
                    Console.Write($"{m} ");
                }
                m++;
            }
        }
    }
}
