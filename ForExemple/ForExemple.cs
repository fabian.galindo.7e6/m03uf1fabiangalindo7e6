﻿/*
* AUTHOR: Fabian Galindo de Oliveira
* DATE: 2022/10/11
* DESCRIPTION: Explicacio dela funció 'for'.
*/
using System;

namespace ForExemple
{
    class ForExemple
    {
        static void Main(string[] args)
        {
            int i;

            for (i = 1; i <= 10; i++)
            {
                Console.WriteLine("Taula de multiplicar" + i);
                for (int j = 0; j <= 10; j++)
                {
                    Console.WriteLine(i + "x" + j + "=" + i * j);
                }
                //Clica enter per continuar
                Console.ReadLine();
            }
        }
    }
}
