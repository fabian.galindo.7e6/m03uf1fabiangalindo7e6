﻿/*
* AUTHOR: Fabian Galindo de Oliveira
* DATE: 2022/10/05
* DESCRIPTION: Farem un programa que demana repetidament a l'usuari un enter fins que entri el número
*/
using System;

namespace WaitForFive
{
    class WaitForFive
    {
        static void Main(string[] args)
        {
            int num = 5;
            Console.WriteLine("Sort per tobar el numero ocult: ");
            int numUsuari = Convert.ToInt32(Console.ReadLine());

            while(numUsuari != 5)
            {
                Console.WriteLine("Enacara no l'has trobat: ");
                numUsuari = Convert.ToInt32(Console.ReadLine());
            }

            Console.WriteLine(num + " trobat!");
        }
    }
}
