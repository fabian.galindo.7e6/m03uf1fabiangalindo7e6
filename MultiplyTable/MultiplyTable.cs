﻿/*
* AUTHOR: Fabian Galindo de Oliveira
* DATE: 2022/10/05
* DESCRIPTION: Mostra per pantalla les taules de multiplicar printat per l'usuari.
*/
using System;

namespace MultiplyTable
{
    class MultiplyTable
    {
        static void Main(string[] args)
        {
            Console.Write("Introdueix el numero per fer la taula: ");

            int i = 0;
            int num = Convert.ToInt32(Console.ReadLine());


            while (i < 10) 
            {
                i++;
                int resultat = i * num;
                Console.WriteLine($"{i} * {num} = {resultat}");
            }
        }
    }
}
