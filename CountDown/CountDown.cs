﻿/*
* AUTHOR: Fabian Galindo de Oliveira
* DATE: 2022/10/11
* DESCRIPTION: L'usuari introdueix un enter (N) i es mostra per pantalla un compte enrere de N fins a 1.
*/
using System;

namespace CountDown
{
    class CountDown
    {
        static void Main(string[] args)
        {
            int i;
            Console.WriteLine("Compre enrere del numero que introduexis.");

            int numUsuari = Convert.ToInt32(Console.ReadLine());
        
            for(i = numUsuari; i > 0; i--)
            {
                Console.Write("{0} ", i);
            }
        }
    }
}
