﻿/*
* AUTHOR: Fabian Galindo de Oliveira
* DATE: 2022/10/11
* DESCRIPTION: Imprimeix per pantalla tants punts com l'usuari hagi indicat.
*/
using System;

namespace DotLine
{
    class DotLine
    {
        static void Main(string[] args)
        {
            int i;
            Console.WriteLine("Introdueix un enter, despres et mostrarà el mateix numero amb '.' (punts).");

            int numUsuari = Convert.ToInt32(Console.ReadLine());

            for (i=0; i < numUsuari; i++) {
                Console.Write(".");
            }
        }
    }
}
