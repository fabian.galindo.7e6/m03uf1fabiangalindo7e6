﻿/*
* AUTHOR: Fabian Galindo de Oliveira
* DATE: 2022/10/25
* DESCRIPTION: S'imprimeix una piràmide d'altura N de #
*/
using System;

namespace _14Piramid
{
    internal class Piramid
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Entra un nombre per ferr una piràmide d'aquesta altura.");
            int Altura = Convert.ToInt32(Console.ReadLine());
            int fila, columna;

            for (fila = 0; fila < Altura; fila++)
            {
                for (columna = 0; columna <= fila; columna++)
                {
                    Console.Write("#");
                }
                Console.WriteLine();
            }
        }
    }
}
