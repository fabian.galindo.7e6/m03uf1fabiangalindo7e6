﻿/*
* AUTHOR: Fabian Galindo de Oliveira
* DATE: 2022/10/05
* DESCRIPTION: Volem comptar quantes línies té un text introduït per l'usuari.
* Versió Do While
*/
using System;

namespace HowManyLinesDoWhile
{
    class HowManyLinesDoWhile
    {
        static void Main(string[] args)
        {
            int i = 0;
            string text = "END";
            string textUsuari = Console.ReadLine();

            do
            {
                i++;
                textUsuari = Console.ReadLine();

            }   while (text != textUsuari);

            Console.WriteLine("Has escrit {0} línies", i);
        }
    }
}
