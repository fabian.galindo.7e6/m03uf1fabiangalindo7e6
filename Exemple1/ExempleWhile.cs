﻿using System;

namespace Exemple1
{
    class ExempleWhile
    {
        static void Main(string[] args)
        {
            int num = 0;

            while(num < 100000) 
            {
                num++;
                Console.Write(num + " ");
            }
        }
    }
}
